<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-blocklist-catalog-builder library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Blocklist\BlocklistCatalogBuilder;
use PhpExtended\Blocklist\BlocklistCatalogListManager;
use PhpExtended\Blocklist\BlocklistCatalogUpdater;
use PhpExtended\Tld\TopLevelDomainHierarchyInterface;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\UriFactoryInterface;
use Psr\Log\LoggerInterface;

/**
 * BlocklistCatalogUpdaterTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Blocklist\BlocklistCatalogUpdater
 *
 * @internal
 *
 * @small
 */
class BlocklistCatalogUpdaterTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var BlocklistCatalogUpdater
	 */
	protected BlocklistCatalogUpdater $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new BlocklistCatalogUpdater(
			$this->getMockForAbstractClass(LoggerInterface::class),
			new BlocklistCatalogListManager(
				$this->getMockForAbstractClass(ClientInterface::class),
				$this->getMockForAbstractClass(UriFactoryInterface::class),
				$this->getMockForAbstractClass(RequestFactoryInterface::class),
				$this->getMockForAbstractClass(LoggerInterface::class),
			),
			new BlocklistCatalogBuilder(
				$this->getMockForAbstractClass(LoggerInterface::class),
				$this->getMockForAbstractClass(TopLevelDomainHierarchyInterface::class),
				$this->getMockForAbstractClass(UriFactoryInterface::class),
			),
		);
	}
	
}
