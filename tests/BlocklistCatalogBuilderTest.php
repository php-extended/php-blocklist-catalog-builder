<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-blocklist-catalog-builder library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Blocklist\BlocklistCatalogBuilder;
use PhpExtended\Tld\TopLevelDomainHierarchyInterface;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\UriFactoryInterface;
use Psr\Log\LoggerInterface;

/**
 * BlocklistCatalogBuilderTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Blocklist\BlocklistCatalogBuilder
 *
 * @internal
 *
 * @small
 */
class BlocklistCatalogBuilderTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var BlocklistCatalogBuilder
	 */
	protected BlocklistCatalogBuilder $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new BlocklistCatalogBuilder(
			$this->getMockForAbstractClass(LoggerInterface::class),
			$this->getMockForAbstractClass(TopLevelDomainHierarchyInterface::class),
			$this->getMockForAbstractClass(UriFactoryInterface::class),
		);
	}
	
}
