#!/bin/sh
# script for php-extended/php-blocklist-catalog-builder
# update of the blocklist lists and blocked domains for php-blocklist-catalog

# change directory to __DIR__
cd `dirname "$0"`

composer update

php updateweekly.php

git push git@gitlab.com:php-extended/php-blocklist-catalog-builder
git push --tags git@gitlab.com:php-extended/php-suffix-list-bundle

if [ -d "../php-blocklist-catalog/.git" ] ; then
	cd "../php-blocklist-catalog"
	git push git@gitlab.com:php-extended/php-blocklist-catalog
	git push --tags git@gitlab.com:php-extended/php-blocklist-catalog
fi;
