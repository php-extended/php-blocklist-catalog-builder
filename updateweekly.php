<?php declare(strict_types=1);

use PhpExtended\Blocklist\BlocklistCatalogBuilder;
use PhpExtended\Blocklist\BlocklistCatalogListManager;
use PhpExtended\Blocklist\BlocklistCatalogUpdater;
use PhpExtended\Logger\BasicConsoleLogger;

require __DIR__.'/vendor/autoload.php';

$logger = new BasicConsoleLogger();
$listManager = new BlocklistCatalogListManager($logger);
$builder = new BlocklistCatalogBuilder($logger);
$update = new BlocklistCatalogUpdater($logger, $listManager, $builder);
$update->doUpdate();
