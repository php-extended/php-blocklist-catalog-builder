<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-blocklist-catalog-builder library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Blocklist;

use FilesystemIterator;
use PhpExtended\Tld\TopLevelDomainHierarchyInterface;
use Psr\Http\Message\UriFactoryInterface;
use Psr\Log\LoggerInterface;
use RuntimeException;
use SplFileInfo;
use Stringable;

/**
 * BlocklistCatalogBuilder class file.
 * 
 * This class is made to transform the downloaded lists into canonicalized
 * domain lists for efficient filtering.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 */
class BlocklistCatalogBuilder implements Stringable
{
	
	/**
	 * The logger.
	 * 
	 * @var LoggerInterface
	 */
	protected LoggerInterface $_logger;
	
	/**
	 * The tld hierarchy.
	 * 
	 * @var TopLevelDomainHierarchyInterface
	 */
	protected TopLevelDomainHierarchyInterface $_tldHierarchy;
	
	/**
	 * The uri factory.
	 * 
	 * @var UriFactoryInterface
	 */
	protected UriFactoryInterface $_uriFactory;
	
	/**
	 * The blocklist catalog path for the data folder.
	 * 
	 * @var string
	 */
	protected string $_blocklistCatalogDataPath;
	
	/**
	 * The blocklist domains data path for the data folder.
	 * 
	 * @var string
	 */
	protected string $_blocklistDomainsDataPath;
	
	/**
	 * The list of domains that is constructing.
	 * 
	 * @var array<string, array<int, string>>
	 */
	private array $_domains = [];
	
	/**
	 * Builds a new BlocklistCatalogBuilder with the given file path.
	 * 
	 * @param LoggerInterface $logger
	 * @param TopLevelDomainHierarchyInterface $tldHierarchy
	 * @param UriFactoryInterface $uriFactory
	 * @throws RuntimeException if the blocklist catalog data path is not
	 *                          where is is expected to be
	 */
	public function __construct(LoggerInterface $logger, TopLevelDomainHierarchyInterface $tldHierarchy, UriFactoryInterface $uriFactory)
	{
		$this->_logger = $logger;
		$this->_tldHierarchy = $tldHierarchy;
		$this->_uriFactory = $uriFactory;
		$this->_blocklistCatalogDataPath = \dirname(__DIR__).\DIRECTORY_SEPARATOR.'data';
		$this->_blocklistDomainsDataPath = \dirname(__DIR__, 2).\DIRECTORY_SEPARATOR.'php-blocklist-catalog'.\DIRECTORY_SEPARATOR.'data';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Updates all the downloaded data lists to the domains category.
	 * 
	 * @return BlocklistCatalogUpdateReport the report of the import
	 * @throws RuntimeException if the file lists cannot be read or written
	 */
	public function updateDomains() : BlocklistCatalogUpdateReport
	{
		$report = new BlocklistCatalogUpdateReport();
		$listDirPath = $this->_blocklistCatalogDataPath.\DIRECTORY_SEPARATOR.'lists';
		if(!\is_dir($listDirPath))
		{
			throw new RuntimeException(\strtr('Failed to find blocklist catalog lists directory, expected to be at {path}', ['{path}' => $listDirPath]));
		}
		
		$listDirectoryIterator = new FilesystemIterator(
			$listDirPath,
			FilesystemIterator::CURRENT_AS_FILEINFO
			| FilesystemIterator::KEY_AS_PATHNAME
			| FilesystemIterator::UNIX_PATHS
			| FilesystemIterator::SKIP_DOTS,
		);
		
		foreach($listDirectoryIterator as $listFileInfo)
		{
			/** @var SplFileInfo $listFileInfo */
			$this->_logger->info('Processing list {fname}', ['fname' => $listFileInfo->getFilename()]);
			$listFileContents = \file_get_contents($listFileInfo->getPathname());
			if(false === $listFileContents)
			{
				throw new RuntimeException(\strtr('Failed to get contents of the list at path {path}', ['{path}' => $listFileInfo->getPathname()]));
			}
			
			$listFileContents = \str_replace("\r\n", "\n", $listFileContents);
			$listContents = \explode("\n", $listFileContents);
			
			foreach($listContents as $listFileLine)
			{
				foreach($this->extractDomains($report, $listFileLine) as $domain)
				{
					$this->registerDomain($domain, $listFileLine);
					$report->incrementProcessedLines();
				}
			}
		}
		
		$domainsDirPath = $this->_blocklistDomainsDataPath;
		
		foreach($this->_domains as $domainPart => $subdomains)
		{
			$tldPath = $domainsDirPath.\DIRECTORY_SEPARATOR.\str_replace('*', '_', $domainPart).'.txt';
			\sort($subdomains);
			$subdomains = \array_unique($subdomains);
			$report->incrementWrittenDomains(\count($subdomains));
			$res = \file_put_contents($tldPath, \implode("\n", $subdomains)."\n");
			if(false === $res)
			{
				throw new RuntimeException(\strtr('Failed to write file for tld {tld} to path {path}', ['{tld}' => $domainPart, '{path}' => $tldPath]));
			}
			$report->incrementWrittenTlds();
		}
		
		return $report;
	}
	
	/**
	 * Extracts the domain list from the given data line.
	 * 
	 * @param BlocklistCatalogUpdateReport $report
	 * @param string $listFileLine
	 * @return array<integer, string> the domains
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 */
	public function extractDomains(BlocklistCatalogUpdateReport $report, string $listFileLine) : array
	{
		if(1 > \mb_strlen($listFileLine)) // empty lines
		{
			return [];
		}
		
		$firstChar = (string) \mb_substr($listFileLine, 0, 1);
		if('#' === $firstChar || '!' === $firstChar || ';' === $firstChar) // comments
		{
			return [];
		}
		
		if('::1' === $listFileLine)
		{
			return [];
		}
		
		if('localhost' === $listFileLine)
		{
			return [];
		}
		
		if(3 < \mb_strlen($listFileLine) && '://' === \mb_substr($listFileLine, 0, 3))
		{
			$listFileLine = (string) \mb_substr($listFileLine, 3);
		}
		
		if(2 < \mb_strlen($listFileLine) && '@@' === \mb_substr($listFileLine, 0, 2))
		{
			$listFileLine = (string) \mb_substr($listFileLine, 2);
		}
			
		if(2 < \mb_strlen($listFileLine) && '||' === \mb_substr($listFileLine, 0, 2))
		{
			$listFileLine = (string) \mb_substr($listFileLine, 2);
		}
		
		if(1 < \mb_strlen($listFileLine) && '|' === \mb_substr($listFileLine, 0, 1))
		{
			$listFileLine = (string) \mb_substr($listFileLine, 1);
		}
		
		if(2 < \mb_strlen($listFileLine) && '//' === \mb_substr($listFileLine, 0, 2))
		{
			$listFileLine = (string) \mb_substr($listFileLine, 2);
		}
		
		$pos = \mb_strpos($listFileLine, '^');
		if(false !== $pos)
		{
			$listFileLine = (string) \mb_substr($listFileLine, 0, $pos);
		}
		
		$pos = \mb_strpos($listFileLine, '$');
		if(false !== $pos)
		{
			$listFileLine = (string) \mb_substr($listFileLine, 0, $pos);
		}
		
		$pos = \mb_strpos($listFileLine, '#');
		if(false !== $pos)
		{
			$listFileLine = (string) \mb_substr($listFileLine, 0, $pos);
		}
		
		$pos = \mb_strpos($listFileLine, '/');
		if(false !== $pos)
		{
			$listFileLine = (string) \mb_substr($listFileLine, 0, $pos);
		}
		
		$pos = \mb_strpos($listFileLine, 'domain=');
		if(false !== $pos)
		{
			$listFileLine = (string) \mb_substr($listFileLine, $pos + (int) \mb_strlen('domain='));
		}
		
		if(8 < \mb_strlen($listFileLine) && '0.0.0.0' === \mb_substr($listFileLine, 0, 7))
		{
			$listFileLine = \ltrim((string) \mb_substr($listFileLine, 7));
		}
		
		if(10 < \mb_strlen($listFileLine) && '127.0.0.1' === \mb_substr($listFileLine, 0, 9))
		{
			$listFileLine = \ltrim((string) \mb_substr($listFileLine, 9));
		}
		
		if(3 < \mb_strlen($listFileLine) && '::1' === \mb_substr($listFileLine, 0, 3))
		{
			$listFileLine = \ltrim((string) \mb_substr($listFileLine, 3));
		}
		
		$listFileLine = \str_replace(' ', '|', $listFileLine);
		$domains = [];
		
		foreach(\explode('|', $listFileLine) as $domainBarLine)
		{
			if(2 < \mb_strlen($domainBarLine))
			{
				if('@@' === \mb_substr($domainBarLine, 0, 2))
				{
					$domainBarLine = (string) \mb_substr($domainBarLine, 2);
				}
				
				if('||' === \mb_substr($domainBarLine, 0, 2))
				{
					$domainBarLine = (string) \mb_substr($domainBarLine, 2);
				}
				
				if('//' === \mb_substr($domainBarLine, 0, 2))
				{
					$domainBarLine = (string) \mb_substr($domainBarLine, 2);
				}
			}
			
			$domainBarLine = \str_replace('\\', '', $domainBarLine);
			$domainBarLine = \str_replace('http://', '', $domainBarLine);
			$domainBarLine = \str_replace('wss://', '', $domainBarLine);
			$domainBarLine = \str_replace('ws*://', '', $domainBarLine);
			
			if(3 < \mb_strlen($domainBarLine))
			{
				if('://' === \mb_substr($domainBarLine, 0, 3))
				{
					$domainBarLine = (string) \mb_substr($domainBarLine, 3);
				}
				
				if('::1' === \mb_substr($domainBarLine, 0, 3)) // localhost
				{
					continue;
				}
			}
			
			$pos = \mb_strpos($domainBarLine, '#');
			if(0 < \mb_strlen($domainBarLine) && false !== $pos)
			{
				$domainBarLine = (string) \mb_substr($domainBarLine, 0, $pos);
			}
			
			$pos = \mb_strpos($domainBarLine, ';');
			if(0 < \mb_strlen($domainBarLine) && false !== $pos)
			{
				$domainBarLine = (string) \mb_substr($domainBarLine, 0, $pos);
			}
			
			$pos = \mb_strpos($domainBarLine, '?');
			if(0 < \mb_strlen($domainBarLine) && false !== $pos)
			{
				$domainBarLine = (string) \mb_substr($domainBarLine, 0, $pos);
			}
			
			$pos = \mb_strpos($domainBarLine, '&');
			if(0 < \mb_strlen($domainBarLine) && false !== $pos)
			{
				$domainBarLine = (string) \mb_substr($domainBarLine, 0, $pos);
			}
			
			$pos = \mb_strpos($domainBarLine, '/');
			if(0 < \mb_strlen($domainBarLine) && false !== $pos)
			{
				$domainBarLine = (string) \mb_substr($domainBarLine, 0, $pos);
			}
			
			$pos = \mb_strpos($domainBarLine, '^');
			if(0 < \mb_strlen($domainBarLine) && false !== $pos)
			{
				$domainBarLine = (string) \mb_substr($domainBarLine, 0, $pos);
			}
			
			$pos = \mb_strpos($domainBarLine, '$');
			if(false !== $pos)
			{
				$domainBarLine = (string) \mb_substr($domainBarLine, 0, $pos);
			}
			
			if(0 < \mb_strlen($domainBarLine) && '.' === \mb_substr($domainBarLine, -1))
			{
				$domainBarLine .= '*';
			}
			
			if(1 > \mb_strlen($domainBarLine)) // empty lines
			{
				continue;
			}
			
			foreach(\explode(',', $domainBarLine) as $domainLine)
			{
// 				if(strlen($domainLine) > 0 && substr($domainLine, 0, 1) === '~')
// 				{
// 					continue;
// 				}
				
// 				if(strlen($domainLine) > 3 && substr($domainLine, 0, 3) === '://')
// 				{
// 					$domainLine = substr($domainLine, 3);
// 				}
				
// 				if(strlen($domainLine) > 0 && ($pos = strpos($domainLine, '^')) !== false)
// 				{
// 					$domainLine = substr($domainLine, 0, $pos);
// 				}
				
// 				if(strlen($domainLine) < 1) // empty lines
// 					continue;
				
				$domains[] = $domainLine;
				$report->incrementProcessedLines();
			}
		}
		
		return $domains;
	}
		
	/**
	 * Registers the given domain value in the known domains.
	 * 
	 * @param string $domainLine
	 * @param string $debugOriginFileLine
	 * @return boolean
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 */
	public function registerDomain(string $domainLine, string $debugOriginFileLine) : bool
	{
		$parts = \explode('.', $domainLine);
		$ascParts = [];
		
		foreach($parts as $part)
		{
			if(empty($part))
			{
				$part = '*';
			}
			
			if('.' === $part[(int) \mb_strlen($part) - 1])
			{
				$part .= '*';
			}
			
			if('-' === $part[(int) \mb_strlen($part) - 1])
			{
				$part .= '*';
			}
			
			if('-' === $part[0])
			{
				$part = '*'.$part;
			}
			
			if('.' === $part[0])
			{
				$part = '*'.$part;
			}
			
			$idnasc = \idn_to_ascii($part, \IDNA_DEFAULT, \INTL_IDNA_VARIANT_UTS46);
			if(false === $idnasc)
			{
				throw new RuntimeException(\strtr('Failed to convert idn to ascii : (part = "{part}") """ {domain} """ [ {orig} ]', ['{part}' => $part, '{domain}' => $domainLine, '{orig}' => $debugOriginFileLine]));
			}
			
			$ascParts[] = $idnasc;
		}
		
		$idndom = \implode('.', $ascParts);
		if(\preg_match('#[^*A-Za-z0-9_.-]#', $idndom))
		{
			throw new RuntimeException(\strtr('There is unallowed characters in domain : {idn} : """ {domain} """ [ {orig} ]', ['{idn}' => $idndom, '{domain}' => $idndom, '{orig}' => $debugOriginFileLine]));
		}
		
		$tld = $parts[\count($ascParts) - 1];
		$tld = \trim($tld);
		if('localhost' === $tld)
		{
			return false;
		}
		
		if('*' !== $tld)
		{
			$tld = \str_replace('*', '', $tld);
		}
		
		$utftld = \idn_to_utf8($tld, \IDNA_DEFAULT, \INTL_IDNA_VARIANT_UTS46);
		if(false === $utftld)
		{
			throw new RuntimeException(\strtr('Failed to convert idn to utf8 : {idn} : """ {domain} """ [ {orig} ]', ['{idn}' => $idndom, '{domain}' => $domainLine, '{orig}' => $debugOriginFileLine]));
		}
		
		$isTld = '*' === $utftld || $this->_tldHierarchy->isTld($utftld);
		if(!$isTld)
		{
			$this->_logger->warning(
				'Ignored tld value (not in the public suffix list) : """ {tld} """ [ {orig} ]',
				['{tld}' => $utftld, '{orig}' => $debugOriginFileLine],
			);
			
			return false;
		}
		
		$matches = [];
		if(!\preg_match('#^((xn--)?[a-z0-9]+)|\\*$#u', $tld, $matches))
		{
// 			\var_dump($tld);
// 			\var_dump($domainLine);
// 			\var_dump($debugOriginFileLine);
// 			\debug_print_backtrace();
			$this->_logger->warning(
				'Ignored tld value {tld} [ {orig} ]',
				['{tld}' => $tld, '{orig}' => $debugOriginFileLine],
			);
			
			throw new RuntimeException(\strtr('Failed to parse tld from value """ {value} """ [ {orig} ]', ['{value}' => $tld, '{orig}' => $debugOriginFileLine]));
		}
		
// 		$asc_parts = array_slice($asc_parts, 0, -1);
// 		foreach($asc_parts as $k => $asc_part)
// 		{
// 			$matches = [];
// 			if(!preg_match('#^(\*|((--|-)\*?)?[A-Za-z0-9]+\*?[A-Za-z0-9]*)(--?(\*|\*?[A-Za-z0-9]+\*?[A-Za-z0-9]*))*$#u', $asc_part, $matches))
// 			{
// 				var_dump($asc_part);
// 				var_dump($debugOriginFileLine);
// 				throw new \RuntimeException(strtr('Failed to parse {k}th domain part """ {value} """ [ {orig} ]',
// 					['{k}' => $k + 1, '{value}' => $asc_part, '{orig}' => $debugOriginFileLine]));
// 			}
// 		}
		
		$this->_domains[$tld][] = \implode('.', $ascParts);
		
		return true;
	}
	
}
