<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-blocklist-catalog-builder library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Blocklist;

use Stringable;

/**
 * BlocklistCatalogUpdateReport class file.
 * 
 * This class aggregate statistics about the manipulations that are done
 * by the catalog update process.
 * 
 * @author Anastaszor
 */
class BlocklistCatalogUpdateReport implements Stringable
{
	
	/**
	 * The number of processed lines.
	 * 
	 * @var integer
	 */
	protected int $_processedLines = 0;
	
	/**
	 * The number of written domains.
	 * 
	 * @var integer
	 */
	protected int $_writtenDomains = 0;
	
	/**
	 * The number of written tls.
	 * 
	 * @var integer
	 */
	protected int $_writtenTlds = 0;
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Adds one to the processed lines count.
	 * 
	 * @return BlocklistCatalogUpdateReport
	 */
	public function incrementProcessedLines() : BlocklistCatalogUpdateReport
	{
		$this->_processedLines++;
		
		return $this;
	}
	
	/**
	 * Gets the number of processed lines.
	 * 
	 * @return integer
	 */
	public function getProcessedLines() : int
	{
		return $this->_processedLines;
	}
	
	/**
	 * Adds to the written domains count.
	 * 
	 * @param integer $count
	 * @return BlocklistCatalogUpdateReport
	 */
	public function incrementWrittenDomains(int $count) : BlocklistCatalogUpdateReport
	{
		$this->_writtenDomains += $count;
		
		return $this;
	}
	
	/**
	 * Gets the number of written lines.
	 * 
	 * @return integer
	 */
	public function getWrittenLines() : int
	{
		return $this->_writtenDomains;
	}
	
	/**
	 * Adds one to the written tld count.
	 * 
	 * @return BlocklistCatalogUpdateReport
	 */
	public function incrementWrittenTlds() : BlocklistCatalogUpdateReport
	{
		$this->_writtenTlds++;
		
		return $this;
	}
	
	/**
	 * Gets the number of written tlds.
	 * 
	 * @return integer
	 */
	public function getWrittenTlds() : int
	{
		return $this->_writtenTlds;
	}
	
}
