<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-blocklist-catalog-builder library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Blocklist;

use Psr\Log\LoggerInterface;
use RuntimeException;
use Stringable;

/**
 * BlocklistCatalogUpdater class file.
 * 
 * This class represents the way to update the file by reloading from the
 * source websites the lists and build the blocklists, and updates the git
 * repositories with the newest information.
 * 
 * @author Anastaszor
 */
class BlocklistCatalogUpdater implements Stringable
{
	
	/**
	 * The logger.
	 * 
	 * @var LoggerInterface
	 */
	protected LoggerInterface $_logger;
	
	/**
	 * The catalog manager.
	 * 
	 * @var BlocklistCatalogListManager
	 */
	protected BlocklistCatalogListManager $_catalogManager;
	
	/**
	 * The catalog builder.
	 * 
	 * @var BlocklistCatalogBuilder
	 */
	protected BlocklistCatalogBuilder $_catalogBuilder;
	
	/**
	 * The path of the php-blocklist-catalog repository.
	 * 
	 * @var string
	 */
	protected string $_catalogPath;
	
	/**
	 * The tag value.
	 * 
	 * @var ?string (int-int-int)
	 */
	private ?string $_actualTag = null;
	
	/**
	 * Builds a new BlocklistCatalogUpdater class file.
	 * 
	 * @param LoggerInterface $logger
	 * @param BlocklistCatalogListManager $listManager
	 * @param BlocklistCatalogBuilder $builder
	 */
	public function __construct(
		LoggerInterface $logger,
		BlocklistCatalogListManager $listManager,
		BlocklistCatalogBuilder $builder
	) {
		$this->_logger = $logger;
		$this->_catalogManager = $listManager;
		$this->_catalogBuilder = $builder;
		$this->_catalogPath = \dirname(__DIR__, 2).'/php-blocklist-catalog';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Does the updates.
	 * @throws RuntimeException
	 */
	public function doUpdate() : void
	{
		$this->_actualTag = $this->getActualTagFromGit();
		$this->buildLists();
		$this->updateReadme();
		$this->commitToGit();
		$this->updateTagToGit();
		$this->updateCatalogReadme();
		$this->commitCatalogToGit();
		$this->updateCatalogToGit();
	}
	
	/**
	 * Gets the actual tag version from git command.
	 * 
	 * @return string
	 * @throws RuntimeException
	 */
	protected function getActualTagFromGit() : string
	{
		$this->_logger->info('Getting actual tag from git...');
		\chdir(\dirname(__DIR__));
		$res = (string) \exec('git describe --tags --abbrev=0');
		$matches = [];
		$this->_logger->info('Found {res}', ['res' => $res]);
		if(!\preg_match('#^(\\d+)\\.(\\d+)\\.(\\d+)#', $res, $matches))
		{
			throw new RuntimeException('Impossible to parse tag '.$res);
		}
		
		/** @psalm-suppress PossiblyUndefinedIntArrayOffset */
		return $matches[1].'-'.$matches[2].'-'.$matches[3];
	}
	
	/**
	 * Updates and builds the blocked domain lists.
	 * 
	 * @throws RuntimeException
	 */
	protected function buildLists() : void
	{
		$this->_catalogManager->updateLists();
		$this->_catalogBuilder->updateDomains();
	}
	
	/**
	 * Updates the readme of this repository.
	 * 
	 * @throws RuntimeException
	 */
	protected function updateReadme() : void
	{
		$this->_logger->info('Updating Readme...');
		$readme = \file_get_contents(\dirname(__DIR__).'/README.md');
		if(false === $readme)
		{
			throw new RuntimeException('Impossible to read readme.');
		}
		
		$readme = \preg_replace('#Last Updated Date : \\d{4}-\\d{2}-\\d{2}#ui', 'Last Updated Date : '.\date('Y-m-d'), $readme);
		if(null === $readme)
		{
			throw new RuntimeException('Impossible to transform readme.');
		}
		
		$count = \file_put_contents(\dirname(__DIR__).'/README.md', $readme);
		if(false === $count)
		{
			throw new RuntimeException('Impossible to write readme.');
		}
		
		$this->_logger->info("\tSuccess");
	}
	
	/**
	 * Updates the readme catalog readme repository.
	 * 
	 * @throws RuntimeException
	 */
	protected function updateCatalogReadme() : void
	{
		$this->_logger->info('Updating Catalog Readme...');
		$readme = \file_get_contents($this->_catalogPath.'/README.md');
		if(false === $readme)
		{
			throw new RuntimeException('Impossible to read readme.');
		}
		
		$readme = \preg_replace('#Last Updated Date : \\d{4}-\\d{2}-\\d{2}#ui', 'Last Updated Date : '.\date('Y-m-d'), $readme);
		if(null === $readme)
		{
			throw new RuntimeException('Impossible to transform readme.');
		}
		
		$count = \file_put_contents($this->_catalogPath.'/README.md', $readme);
		if(false === $count)
		{
			throw new RuntimeException('Impossible to write readme.');
		}
		
		$this->_logger->info("\tSuccess");
	}
	
	/**
	 * Adds the commit to git.
	 */
	protected function commitToGit() : void
	{
		$this->_logger->info('Committing to git...');
		\chdir(\dirname(__DIR__));
		\exec('git add --all');
		\exec('git commit -m "Automatic update at '.\date('Y-m-d').'"');
		$this->_logger->info("\tSuccess");
	}
	
	/**
	 * Adds the commit catalog to git.
	 */
	protected function commitCatalogToGit() : void
	{
		$this->_logger->info('Committing to git...');
		\chdir(\dirname($this->_catalogPath));
		\exec('git add --all');
		\exec('git commit -m "Automatic update at '.\date('Y-m-d').'"');
		$this->_logger->info("\tSuccess");
	}
	
	/**
	 * Tags current commit with the newest tag value.
	 * 
	 * @throws RuntimeException
	 */
	protected function updateTagToGit() : void
	{
		$this->_logger->info('Updating git tag...');
		\chdir(\dirname(__DIR__));
		$matches = [];
		if(!\preg_match('#(\\d+)-(\\d+)-(\\d+)#', (string) $this->_actualTag, $matches))
		{
			throw new RuntimeException('Impossible to parse git tag');
		}
		
		/** @psalm-suppress PossiblyUndefinedIntArrayOffset */
		$newtag = $matches[1].'.'.$matches[2].'.'.((string) (((int) $matches[3]) + 1));
		
		\exec('git tag -a "'.$newtag.'" -m "Automatic update at '.\date('Y-m-d').'"');
		$this->_logger->info("\nSuccess");
	}
	
	/**
	 * Tags current commit with the newest tag value.
	 * 
	 * @throws RuntimeException
	 */
	protected function updateCatalogToGit() : void
	{
		$this->_logger->info('Updating catalog git tag...');
		\chdir(\dirname($this->_catalogPath));
		$matches = [];
		if(!\preg_match('#(\\d+)-(\\d+)-(\\d+)#', (string) $this->_actualTag, $matches))
		{
			throw new RuntimeException('Impossible to parse git tag');
		}
		
		/** @psalm-suppress PossiblyUndefinedIntArrayOffset */
		$newtag = $matches[1].'.'.$matches[2].'.'.((string) (((int) $matches[3]) + 1));
		
		\exec('git tag -a "'.$newtag.'" -m "Automatic update at '.\date('Y-m-d').'"');
		$this->_logger->info("\nSuccess");
	}
	
}
