<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-blocklist-catalog-builder library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Blocklist;

use InvalidArgumentException;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\UriFactoryInterface;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Stringable;

/**
 * BlocklistCatalogListManager class file.
 * 
 * This class is made to download all available lists and store them.
 * 
 * @author Anastaszor
 */
class BlocklistCatalogListManager implements Stringable
{
	
	/**
	 * The logger.
	 * 
	 * @var LoggerInterface
	 */
	protected LoggerInterface $_logger;
	
	/**
	 * The uri factory.
	 * 
	 * @var UriFactoryInterface
	 */
	protected UriFactoryInterface $_uriFactory;
	
	/**
	 * The request factory.
	 * 
	 * @var RequestFactoryInterface
	 */
	protected RequestFactoryInterface $_requestFactory;
	
	/**
	 * The http client.
	 * 
	 * @var ClientInterface
	 */
	protected ClientInterface $_client;
	
	/**
	 * The blocklist catalog path for the data folder.
	 * 
	 * @var string
	 */
	protected string $_blocklistCatalogDataPath;
	
	/**
	 * Builds a new BlocklistCatalogListManager with the given http client
	 * and dependancies.
	 * 
	 * @param ClientInterface $client
	 * @param UriFactoryInterface $uriFactory
	 * @param RequestFactoryInterface $requestFactory
	 * @param LoggerInterface $logger
	 * @throws RuntimeException if the blocklist catalog data path is not
	 *                          where it is expected to be
	 */
	public function __construct(ClientInterface $client, UriFactoryInterface $uriFactory, RequestFactoryInterface $requestFactory, LoggerInterface $logger)
	{
		$this->_logger = $logger;
		$this->_requestFactory = $requestFactory;
		$this->_uriFactory = $uriFactory;
		$this->_client = $client;
		$this->_blocklistCatalogDataPath = \dirname(__DIR__).\DIRECTORY_SEPARATOR.'data';
		if(!\is_dir($this->_blocklistCatalogDataPath))
		{
			throw new RuntimeException(\strtr('Failed to find blocklist catalog data path at {path}', ['{path}' => $this->_blocklistCatalogDataPath]));
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Updates all the lists in the data.lists folder of the blocklist catalog.
	 * 
	 * @return integer the number of lists updated
	 * @throws RuntimeException if the file lists cannot be read or written
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function updateLists() : int
	{
		$listFilePath = $this->_blocklistCatalogDataPath.\DIRECTORY_SEPARATOR.'list_of_lists.txt';
		if(!\is_file($listFilePath))
		{
			throw new RuntimeException(\strtr('Failed to find blocklist catalog data list of lists, expected to be at {path}', ['{path}' => $listFilePath]));
		}
		
		$listFileContents = \file_get_contents($listFilePath);
		if(false === $listFileContents)
		{
			throw new RuntimeException(\strtr('Failed to get contents of the blocklist catalog data list of lists at {path}', ['{path}' => $listFilePath]));
		}
		
		$listContents = \explode("\n", $listFileContents);
		$count = 0;
		
		foreach($listContents as $listFileLine)
		{
			if(1 > \mb_strlen($listFileLine)) // empty lines
			{
				continue;
			}
			
			if('#' === \mb_substr($listFileLine, 0, 1)) // comments
			{
				continue;
			}
			
			try
			{
				$uri = $this->_uriFactory->createUri($listFileLine);
			}
			catch(InvalidArgumentException $exc)
			{
				$this->_logger->error(
					'Failed to parse uri : {uri}',
					['uri' => $listFileLine],
				);
				$this->_logger->error($exc->getMessage());
				continue;
			}
			
			$request = $this->_requestFactory->createRequest('GET', $uri);
			
			try
			{
				$response = $this->_client->sendRequest($request);
			}
			catch(ClientExceptionInterface $exc)
			{
				$this->_logger->error(
					'Failed to get list from url : {uri}',
					['uri' => $uri->__toString()],
				);
				$this->_logger->error($exc->getMessage());
				continue;
			}
			
			if(200 !== $response->getStatusCode())
			{
				$this->_logger->error(
					'The given response for uri {uri} has status code {code} {reason}',
					['uri' => $uri->__toString(), 'code' => $response->getStatusCode(), 'reason' => $response->getReasonPhrase()],
				);
			}
			
			$data = $response->getBody()->__toString();
			if(empty($data))
			{
				$this->_logger->error(
					'The given response for uri {uri} is empty.',
					['uri' => $uri->__toString()],
				);
			}
			
			$fileName = (string) \preg_replace('#[^a-z0-9]#', '-', (string) \mb_strtolower($uri->__toString()));
			$filePath = $this->_blocklistCatalogDataPath.\DIRECTORY_SEPARATOR.'lists'.\DIRECTORY_SEPARATOR.$fileName;
			$bytes = \file_put_contents($filePath, $data);
			if(false === $bytes || 0 === $bytes)
			{
				throw new RuntimeException(\strtr('Failed to write block list data for uri {uri} at {path}', ['{uri}' => $uri->__toString(), '{path}' => $filePath]));
			}
			
			$count++;
		}
		
		return $count;
	}
	
}
