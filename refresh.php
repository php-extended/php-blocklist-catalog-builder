<?php declare(strict_types=1);

/**
 * Update script for the blocklist catalog builder.
 * 
 * Usage:
 * php refresh.php                 // downloads, then builds
 * php refresh.php --skip-download // only builds
 * php refresh.php --skip-build    // only downloads
 * 
 * @author Anastaszor
 */

use PhpExtended\Logger\BasicConsoleLogger;
use PhpExtended\Blocklist\BlocklistCatalogListManager;
use PhpExtended\Blocklist\BlocklistCatalogBuilder;

global $argv;

$composer = __DIR__.'/vendor/autoload.php';
if(!is_file($composer))
{
	throw new RuntimeException('You should run composer first.');
}
require $composer;

$logger = new BasicConsoleLogger(3);

$skip = isset($argv[1]) && $argv[1] === '--skip-download';
if(!$skip)
{
	$listManager = new BlocklistCatalogListManager($logger);
	$c = $listManager->updateLists();
	$logger->info('Successfully Downloaded {k} lists', ['k' => $c]);
}

$skip = isset($argv[1]) && $argv[1] === '--skip-build';
if(!$skip)
{
	$builder = new BlocklistCatalogBuilder($logger);
	$c = $builder->updateDomains();
	$logger->info('Successfully Builded {k} domains for {t} tlds ({l} lines parsed)',
		['k' => $c->getWrittenLines(), 't' => $c->getWrittenTlds(), 'l' => $c->getProcessedLines()]);
}
